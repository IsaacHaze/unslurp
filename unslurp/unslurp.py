"""unslurp

Index a jsons file/dump


usage:
  unslurp.py [options] FILE

options:
  --id-field FIELD    Name of the field which is to be considered the _id
                      [default: None]
  --index INDEX       Name of the index
  --type TYPE         Document type  [default: document]
  --create MODE       Set creation of indices, must be one of: no, yes, force.
                      "yes" will create the index if it doesn't exist,
                      "force" will first delete the target index and then
                      re-create it  [default: no]
  --mappings FILE     JSON file containing the index field mappings
  --settings FILE     JSON file containing the index settings
  --chunk-size SIZE   Chunk or batch size to use for bulk indexing [default: 500]
  --skip NUM          Skip the first NUM documents  [default: 0]
  --host HOST         Specify elasticsearch host  [default: localhost]
  --port PORT         Specify elasticsearch port  [default: 9200]
  -h, --help          print help
  -v, --version       print version

"""
import sys
import logging
from itertools import imap, islice

from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk, BulkIndexError
from docopt import docopt
import ujson as json


log = logging.getLogger(__name__)


class IdFieldMissingException(BaseException):
    pass


def read_jsons(fname):
    with open(fname) as f:
        for i, line in enumerate(f, start=1):
            try:
                doc = json.loads(line)
            except ValueError as e:
                log.error('exception loading line %i: %s', i, e)
                continue
            yield doc


class Unslurp(object):
    def __init__(self, host, port, index, doc_type, chunk_size=None,
                 id_field=None, mappings=None, settings=None, create=None):
        self.es_index = index
        self.es_doc_type = doc_type
        if create is None:
            create = 'no'
        self.create = create
        self.id_field = id_field
        self.chunk_size = chunk_size
        self.mappings = mappings
        self.settings = settings
        self.es = Elasticsearch(host=host, port=port)

    @staticmethod
    def read_json_cfg(fname, field=None):
        assert field is not None, 'you must specify the top-level configuration field to read'

        if fname is None:
            return None
        with open(fname) as f:
            obj = json.loads(f.read())
        return obj[field]

    def _prep_doc(self, doc):
        """Prepare the documents for bulk indexing into Elasticsearch."""
        doc['_op_type'] = 'index'
        doc['_index'] = self.es_index
        doc['_type'] = self.es_doc_type
        if self.id_field is not None:
            try:
                doc['_id'] = doc[self.id_field]
            except KeyError as e:
                raise IdFieldMissingException(e)
        try:
            del doc['_score']
        except KeyError:
            pass

        return doc

    def create_index(self):
        if self.create == 'no':
            return
        log.info('creating index: %s', self.es_index)

        body = {}
        if self.mappings is not None:
            body["mappings"] = self.mappings
        if self.settings is not None:
            body["settings"] = self.settings

        if self.create == 'force':
            self.es.indices.delete(index=self.es_index, ignore=404)
        self.es.indices.create(index=self.es_index, body=body)

    def index(self, iterable, skip=None):
        """Bulk index the documents in the iterable"""
        self.create_index()

        request_timeout = 30
        if skip is not None:
            log.debug('skipping the first %d documents', skip)
            iterable = islice(iterable, skip, None)
        log.info('indexing documents (chunk_size=%d, request_timeout=%d)',
                  self.chunk_size, request_timeout)

        i = None
        try:
            for i, (is_ok, obj) in enumerate(streaming_bulk(self.es, imap(self._prep_doc,
                                                                          iterable),
                                                            refresh='wait_for',
                                                            chunk_size=self.chunk_size,
                                                            raise_on_exception=False,
                                                            request_timeout=request_timeout)):
                if i % 1000 == 0:
                    log.debug("i: %d", i)
                if is_ok:
                    continue

                log.warn('failed indexing document: %s', obj)
        except BulkIndexError as e:
            log.error('got bulk indexing error at index %d: %d', i, e)


def main():
    args = docopt(__doc__)
    fname = args['FILE']
    port = args['--port']
    host = args['--host']
    index = args['--index']
    create = args['--create']
    doc_type = args['--type']
    id_field = args['--id-field']
    chunk_size = int(args['--chunk-size'])
    mappings = args['--mappings']
    settings = args['--settings']
    skip = int(args['--skip'])

    if id_field == 'None':
        id_field = None

    mappings = Unslurp.read_json_cfg(mappings, field='mappings')
    settings = Unslurp.read_json_cfg(settings, field='settings')

    unslurper = Unslurp(host, port, index, id_field=id_field,
                        doc_type=doc_type, chunk_size=chunk_size,
                        mappings=mappings, settings=settings, create=create)

    jsons = read_jsons(fname)
    try:
        unslurper.index(jsons, skip=skip)
    except IdFieldMissingException as e:
        log.warn("document {} was missing an id field ({}), aborting".format(fname, id_field))
        raise e


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)20s [%(levelname)7s] %(message)s',
                        datefmt='%Y-%m-%dT%H:%M:%S',
                        level=logging.INFO)
    log.setLevel(logging.DEBUG)
    main()
