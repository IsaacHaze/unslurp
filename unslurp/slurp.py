"""slurp

Get documents from an elasticsearch index.

usage:
  slurp.py --host HOST --index INDEX [options] FILE

arguments:
  --host HOST         url of the Elasticsearch host [default: localhost]
  --index INDEX       Name of the index
  FILE                output file

options:
  --type TYPE         Document type [default: document]
  --mapping           retrieve & store mapping [default: False]
  --port PORT         Specify elasticsearch port [default: 9200]
  --query QUERY       Select a subset of the documents from the index
                      [default: {"query": {"match_all": {}}}]
  -h, --help          print help
  -v, --version       print version

"""
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
from docopt import docopt
import json


def slurp(host, index, fname, port=None, query=None, mapping=None):
    if port is None:
        port = 9200

    if query is None:
        query = {"query": {"match_all": {}}}
    else:
        if isinstance(query, basestring):
            query = json.loads(query)

    es = Elasticsearch(host=host, port=port)

    if mapping:
        with open(fname + '.mapping', "w") as out:
            mapping = es.indices.get_mapping(index)[index]
            out.write(json.dumps(mapping, indent=4))

    with open(fname, "w") as out:
        for doc in scan(es, query=query, index=index):
            json.dump(doc["_source"], out)
            out.write("\n")


def main():
    args = docopt(__doc__)
    fname = args['FILE']
    port = args['--port']
    host = args['--host']
    index = args['--index']
    doc_type = args['--type']
    mapping = args['--mapping']
    query = args['--query']

    slurp(host, index, fname, port=port, query=query, mapping=mapping)


if __name__ == '__main__':
    main()
