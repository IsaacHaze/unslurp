# setup.py
from setuptools import setup, find_packages

import unslurp


setup(name='unslurp',
      version=unslurp.__version__,
      description='unslurp',
      url='https://bitbucket.org/IsaacHaze/unslurp',
      author='Isaac Sijaranamual',
      author_email='isaacsijaranamual@gmail.com',
      license='MIT',
      entry_points={
        'console_scripts': [
          'unslurp=unslurp.unslurp:main',
          'slurp=unslurp.slurp:main',
        ]
      },
      install_requires=[
          'docopt',
          'elasticsearch',
          'ujson',
      ],
      include_package_data=False,
      packages=find_packages(),
      zip_safe=True,
)
