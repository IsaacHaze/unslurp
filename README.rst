=======
Unslurp
=======

Small utility to get a set of JSONs in to, or out of, Elasticsearch.


Requirements
============

- python 2.7
- Elasticsearch (https://www.elastic.co/products/elasticsearch)


Installation
============

Install with pip:

.. code:: shell

    $ pip install unslurp


Usage
=====

Inserting/indexing JSONs file(s):

.. code:: shell

    $ unslurp -h
    unslurp

    Index a jsons file/dump


    usage:
      unslurp.py [options] FILE

    options:
      --id-field FIELD    Name of the field which is to be considered the _id
                          [default: None]
      --index INDEX       Name of the index
      --type TYPE         Document type  [default: document]
      --create MODE       Set creation of indices, must be one of: no, yes, force.
                          "yes" will create the index if it doesn't exist,
                          "force" will first delete the target index and then
                          re-create it  [default: no]
      --mappings FILE     JSON file containing the index field mappings
      --settings FILE     JSON file containing the index settings
      --chunk-size SIZE   Chunk or batch size to use for bulk indexing [default: 500]
      --skip NUM          Skip the first NUM documents  [default: 0]
      --host HOST         Specify elasticsearch host  [default: localhost]
      --port PORT         Specify elasticsearch port  [default: 9200]
      -h, --help          print help
      -v, --version       print version


And similarly for retrieving JSONs from Elasticsearch:

.. code:: shell

    $ slurp -h
    slurp

    Get documents from an elasticsearch index.

    usage:
      slurp.py --host HOST --index INDEX [options] FILE

    arguments:
      --host HOST         url of the Elasticsearch host [default: localhost]
      --index INDEX       Name of the index
      FILE                output file

    options:
      --type TYPE         Document type [default: document]
      --mapping           retrieve & store mapping [default: False]
      --port PORT         Specify elasticsearch port [default: 9200]
      --query QUERY       Select a subset of the documents from the index
                          [default: {"query": {"match_all": {}}}]
      -h, --help          print help
      -v, --version       print version
